CLOSET13 is a place we've built specifically for women. What does she need to make her life better, to make her feel good in her skin, to make her be exactly who she wants to be? This place is her new universe.

So, pay attention my darlings out there... this is a store for you to ï¬nd anything you can think of: from the clothes in your closet to the book you keep on your nightstand, and why not some homewear. Tell me you aint already in love with this shop?

We love people who choose to be different! ...that's why CLOSET13 is not about the international brands, is not about the mainstream, but its all about cool designers with a fresh vibe and a free spirit, about all the things a woman might need. When you start shopping here, your closet is our closet, so lets go crazy and ï¬nd you some goodies. Trust us to spice up your closet. All the items we bring to this platform we carefully select so that our clients ï¬nd here a clean, minimalistic and cool style.

Shopping here becomes an experience. Why? Cause if you choose a random item you can easily combine it with any other random one, creating a good, stylish outï¬t to ï¬t your attitude. Even though the designers are different, they are all united here under the same cool vibe.

CLOSET13 shows the world that style is not about uniformity, its about the personal touch of the designer, its about individuality, its about that unique mark you either have or... you dont, in which case we believe the fashion industry may not be for you. So, open CLOSET13 and choose the best items for your home, best items for your everyday life and the most important, some of the clothes that suit you so well, you become one unforgettable presence to the ones around you.

Cheers & Hugs, CLOSET13